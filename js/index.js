$(".menu-list").css({
  position: "absolute",
  top: "70px",
  right: "0",
  visibility: "hidden",
  "background-color": "rgba(35, 35, 35, 1)",
  width: "290px",
  height: "min-content",
});

$(".menu, .menu-link").hover(
  function () {
    $(this).css({ color: "rgba(20, 185, 213, 1)", cursor: "pointer" });
  },
  function () {
    $(this).css("color", "white");
  }
);

$(".menu").click(function () {
  $(".menu-list").toggle(function () {
    $(this).css("visibility", "visible");
  });
});

$(".menu-item").css({
  "text-transform": "uppercase",
  margin: "20px 20px 20px 13px",
});
$("html").css("scroll-behavior", "smooth");

$(".go-up-button").css({
  position: "fixed",
  bottom: "40px",
  right: "40px",
  visibility: "hidden",
  "background-color": "rgba(20, 185, 213, 1)",
  border: "none",
  "border-radius": "5px",
  width: "100px",
  height: "40px",
  color: "white",
});

$(window).scroll(function () {
  if ($(document).scrollTop() > 800) {
    $(".go-up-button").css("visibility", "visible");
  } else {
    $(".go-up-button").css("visibility", "hidden");
  }
});

$(".hide-button").css({
  "background-color": "rgba(20, 185, 213, 1)",
  "align-self": "center",
  border: "none",
  "border-radius": "5px",
  width: "100px",
  height: "40px",
  color: "white",
});

$(document).ready(function () {
  $(".hide-button").click(function () {
    $(".box").slideToggle();
  });
});
